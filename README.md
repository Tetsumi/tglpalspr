tglpalspr
===================

OpenGL technical demo on how to render palette colored sprites.

Needs GLFW3 and GLEW. Compile with `gcc -O3 -lGL -lGLEW -lglfw main.c`

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png) 
