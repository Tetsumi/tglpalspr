/*
  tglpalspr
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2016 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "base.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <smmintrin.h>

DEFINE_TYPEDEFS(GLFWwindow)
DEFINE_TYPEDEFS(GLenum);
DEFINE_TYPEDEFS(GLuint);
DEFINE_TYPEDEFS(GLint);
DEFINE_TYPEDEFS(GLenum);
DEFINE_TYPEDEFS(GLchar);
DEFINE_TYPEDEFS(GLubyte);
DEFINE_TYPEDEFS(GLfloat);

#define WINDOW_WIDTH  800
#define WINDOW_HEIGHT 600
#define WINDOW_TITLE  "tglpalspr"

typedef union
{
	__m128 m128[4];
	R32    elements[16];
} Matrix;

DEFINE_TYPEDEFS(Matrix);

P_GLFWwindow g_win;
GLuint       g_program;
GLuint       g_texture;

Error initialize        (Void);
Void  finalize          (Void);
Error loop              (void);
Error createShader      (CP_GLuint, C_GLenum, CPC_Char);
Error glErrorString     (C_GLenum, CPPC_Char);
Error initializeGl      (Void);
Error initializeProgram (Void);
Error initializeTexture (Void);

Error glErrorString (C_GLenum error, CPPC_Char str)
{
#define CASE_STR(x) case x: *str = #x; break;
	switch (error)
	{
		CASE_STR(GL_NO_ERROR);
		CASE_STR(GL_INVALID_ENUM);
		CASE_STR(GL_INVALID_VALUE);
		CASE_STR(GL_INVALID_OPERATION);
		CASE_STR(GL_STACK_OVERFLOW);
		CASE_STR(GL_STACK_UNDERFLOW);
		CASE_STR(GL_OUT_OF_MEMORY);
		CASE_STR(GL_INVALID_FRAMEBUFFER_OPERATION);
		CASE_STR(GL_CONTEXT_LOST);
	default:
		*str = "UNKNOWN";
		return ERROR_UNKNOWN;
	}
#undef CASE_STR
	return ERROR_NONE;
}

Error createShader (CP_GLuint shader, C_GLenum type, CPC_Char source)
{
        C_GLuint s = glCreateShader(type);
	
	if (!s)
		return ERROR_GL_SHADER;
	
	glShaderSource(s, 1, &source, NULL);

	GLenum glErr = glGetError();

	if (GL_NO_ERROR != glErr)
		return ERROR_GL_SHADER;

	glCompileShader(s);

	GLint shaderIv;
	
	glGetShaderiv(s, GL_COMPILE_STATUS, &shaderIv);

	if (GL_TRUE != shaderIv)
	{
		glGetShaderiv(s, GL_INFO_LOG_LENGTH, &shaderIv);
		Char buffer[shaderIv];
		glGetShaderInfoLog(s, shaderIv, NULL, buffer);
		glDeleteShader(s);
		P_ERROR("Failed to compile shader. Log:\n%s", buffer);
		return ERROR_GL_SHADER;
	}

	*shader = s;
	
	return ERROR_NONE;
}

Error initializeProgram (Void)
{
	CPC_Char vertexShaderSource =
		"#version 450\n"
		"layout(location = 0) uniform mat4 projection;"
		"layout(location = 1) in ivec3 in_Vertex3D;"
		"layout(location = 2) in vec2 in_TexCoord;"
		"out vec2 TexCoord;"
		"void main()"
		"{"
		"    gl_Position = projection * vec4(in_Vertex3D, 1.0);"
		"    TexCoord = in_TexCoord;"
		"}";
	
	GLuint vertexShader = 0;	
	Error e = createShader(&vertexShader,
			       GL_VERTEX_SHADER,
			       vertexShaderSource);
	
	if (ERROR_NONE != e)
	{
		PC_Char estr;
		glErrorString(glGetError(), &estr);
		P_ERROR("Failed to compile vertex shader (%s).", estr);
		return ERROR_GL_SHADER;
	}

	CPC_Char fragmentShaderSource =
		"#version 450\n"
		"layout(binding  = 0) uniform usampler2D texture1;"
		"layout(location = 3) uniform vec4 palette[8];"
		"in vec2 TexCoord;"
		"out vec4 color;"
		"void main()"
		"{"
	        "      color = palette[texture(texture1, TexCoord).r];"
		"}";
	
	GLuint fragmentShader = 0;
	
	e = createShader(&fragmentShader,
			 GL_FRAGMENT_SHADER,
			 fragmentShaderSource);
	
	if (ERROR_NONE != e)
	{
		PC_Char estr;
		glErrorString(glGetError(), &estr);
		P_ERROR("Failed to compile fragment shader (%s).", estr);
		return ERROR_GL_SHADER;
	}

	C_GLuint program = glCreateProgram();

	if (!program)
	{
		PC_Char estr;
		glErrorString(glGetError(), &estr);
		P_ERROR("Failed to create program (%s).", estr);
		return ERROR_GL_PROGRAM;
	}

	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	GLint programIv;

	glGetProgramiv(program, GL_LINK_STATUS, &programIv);

	if (GL_TRUE != programIv)
	{
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &programIv);
		Char buffer[programIv];
		glGetProgramInfoLog(program, programIv, NULL, buffer);
		glDeleteProgram(program);
		PC_Char estr;
		glErrorString(glGetError(), &estr);
		P_ERROR("Failed to link program. (%s) Log:\n%s",
			estr,
			buffer);
		return ERROR_GL_PROGRAM;
	}

	glUseProgram(program);

	g_program = program;

	return ERROR_NONE;
}

Error initializeTexture (Void)
{
       C_GLubyte texturePixel[16][16] = {
		{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0},
		{0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0, 0, 0, 0},
		{0, 0, 0, 1, 1, 2, 2, 2, 2, 2, 2, 2, 1, 0, 0, 0},
		{0, 0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 0, 0, 0},
		{0, 0, 1, 2, 1, 2, 3, 2, 2, 3, 2, 2, 1, 0, 0, 0},
		{0, 0, 0, 1, 1, 3, 3, 2, 3, 3, 3, 2, 1, 0, 0, 0},
		{0, 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 2, 1, 1, 1, 0},
		{1, 2, 2, 2, 1, 3, 1, 3, 3, 1, 3, 1, 2, 2, 2, 1},
		{0, 1, 3, 3, 4, 1, 3, 3, 3, 3, 1, 4, 3, 4, 1, 0},
		{1, 3, 4, 3, 1, 4, 1, 1, 1, 1, 4, 1, 3, 1, 4, 1},
		{1, 3, 3, 1, 1, 4, 4, 3, 3, 4, 4, 1, 1, 3, 3, 1},
		{1, 4, 1, 1, 1, 1, 4, 4, 4, 4, 1, 1, 1, 3, 3, 1},
		{1, 4, 1, 1, 1, 3, 1, 1, 4, 1, 3, 1, 1, 1, 4, 1},
		{1, 4, 4, 1, 1, 4, 4, 1, 1, 3, 3, 1, 1, 1, 4, 1},
		{1, 4, 1, 0, 0, 1, 1, 1, 4, 4, 4 ,1, 0, 1, 4, 1},
		{0, 1, 0, 1, 1, 1, 1, 1, 4, 4, 1, 1, 1, 0, 1, 0}
	};
	
	glGenTextures(1, &g_texture);

	if (!g_texture)
	{
		PC_Char estr;
		glErrorString(glGetError(), &estr);
		P_ERROR("Failed to generate texture (%s)", estr);
		return ERROR_GL_TEXTURE;
	}
	
	glBindTexture(GL_TEXTURE_2D, g_texture);
	glTexImage2D(GL_TEXTURE_2D,
		     0,
		     GL_R8UI,
		     16,
		     16,
		     0,
		     GL_RED_INTEGER,
		     GL_UNSIGNED_BYTE,
		     texturePixel);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, g_texture);
	
	return ERROR_NONE;
}

Error initializeGl (Void)
{
	if (ERROR_NONE != initializeProgram())
	{
		P_ERROR("Failed to initialize Program.");
		return ERROR_GL_PROGRAM;
	}
	
	return initializeTexture();
}

Error initialize (Void)
{
	if (!glfwInit())
	{
		P_ERROR("Failed to initialize GLFW.");
		return ERROR_GLFW;
	}
	
	atexit(finalize);
	
	void errorCallback(S32 e, CPC_Char msg)
	{
		P_ERROR("GLFW Error (%d): %s", e, msg);
	}

	glfwSetErrorCallback(errorCallback);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
	
	g_win = glfwCreateWindow(WINDOW_WIDTH,
				 WINDOW_HEIGHT,
				 WINDOW_TITLE,
				 NULL,
				 NULL);
	if (!g_win)
	{
		P_ERROR("Failed to open a window.");
		return ERROR_GLFW;
	}

	glfwMakeContextCurrent(g_win);

	C_GLenum err = glewInit();

	if (GLEW_OK != err)
	{
		P_ERROR("Failed to initialize GLEW: %s",
			glewGetErrorString(err));
		return ERROR_GLEW;
	}

	return initializeGl();
}

Void finalize (Void)
{
	glfwTerminate();
}

Error loop (void)
{
	C_Matrix orthoMatrix = { .elements = {
			[ 0] =  2.0f / (WINDOW_WIDTH),
			[ 5] =  2.0f / (WINDOW_HEIGHT), 
			[10] = -1.0f,
			[12] = -1.0f,
			[13] = -1.0f,
			[14] =  1.0f,
			[15] =  1.0f
		}};

	glUniformMatrix4fv(0, 1, GL_FALSE, orthoMatrix.elements);

	C_GLint vertices[8] = {
		      96,       44,
		96 + 256,       44,     
		96 + 256, 44 + 256,
		      96, 44 + 256
	};
	
	C_GLint vertices2[8] = {
		      448,       44,
		448 + 256,       44,     
		448 + 256, 44 + 256,
		      448, 44 + 256
	};

	C_GLfloat texCoords[8] = {
		0.0f, 1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f,
		0.0f, 0.0f
	};

	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, texCoords);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(1);        
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	
	C_GLfloat palette[8][4] = {
		[0] = {1.0f,  1.0f,  1.0f,  1.0f},
		[1] = {0.0f,  0.0f,  0.0f,  1.0f},
		[2] = {0.78f, 0.29f, 0.05f, 1.0f},
		[3] = {0.98f, 0.73f, 0.69f, 1.0f},
		[4] = {0.36f, 0.58f, 0.98f, 1.0f}
	};

	C_GLfloat palette2[8][4] = {
		[0] = {1.0f,  1.0f,  1.0f,  1.0f},
		[1] = {0.0f,  0.0f,  0.0f,  1.0f},
		[2] = {0.22f, 0.35f, 0.78f, 1.0f},
		[3] = {0.5f,  0.5f,  0.3f,  1.0f},
		[4] = {0.7f,  0.58f, 0.98f, 1.0f}
	};
	
	while (!glfwWindowShouldClose(g_win))
	{
		glClear(GL_COLOR_BUFFER_BIT);
		glUniform4fv(3, 8, palette[0]);
		glVertexAttribIPointer(1, 2, GL_INT, 0, vertices);
		glDrawArrays(GL_QUADS, 0, 4);
		glUniform4fv(3, 8, palette2[0]);
		glVertexAttribIPointer(1, 2, GL_INT, 0, vertices2);
		glDrawArrays(GL_QUADS, 0, 4);
		glfwSwapBuffers(g_win);
		glfwPollEvents();
	}
	
	return ERROR_NONE;
}
	
int main (Void)
{
	if (ERROR_NONE != initialize())
	{
		P_ERROR("Failed to initialize.");
		return EXIT_FAILURE;
	}
	
	loop();
}
